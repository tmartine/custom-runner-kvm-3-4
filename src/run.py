#!/builds/orchestrator/bin/python

# Standard library
import io
import json
import logging
import os
import shlex
import sys
import typing

# Third-party imports
import fabric
import invoke

# Project module
import base


def main(logger: logging.LoggerAdapter) -> None:
    job_info = base.get_job_info()
    if isinstance(job_info.image.contents, base.Test):
        test(job_info.image.contents)
        return
    vm = base.find_vm_by_job(base.Config.load().get_cloudstack(), job_info)
    vm_ip = base.get_vm_ip_address(vm)
    connection = fabric.Connection(f"ci@{vm_ip}", connect_kwargs={"password": "ci"})
    connect(logger, connection, job_info.image.contents.options.docker)


def test(info: base.Test):
    pass


def docker_login(connection: fabric.Connection, ci_registry: str):
    user = os.environ["CUSTOM_ENV_CI_REGISTRY_USER"]
    password = os.environ["CUSTOM_ENV_CI_REGISTRY_PASSWORD"]
    password_stream = io.StringIO(password)
    connection.run(
        f"docker login --username {user} --password-stdin {ci_registry}",
        in_stream=password_stream,
    )


def connect(
    logger: logging.LoggerAdapter,
    connection: fabric.Connection,
    docker: typing.Optional[str],
):
    command_file = sys.argv[1]
    step_name = sys.argv[2]

    if docker is None:
        use_bash = True
    elif step_name not in [
        "before_script",
        "build_script",
        "step_script",
        "script",
        "after_script",
    ]:
        use_bash = True
    else:
        use_bash = False
    if use_bash:
        # Gitlab runner executes some Gitlab predefined commands (e.g., get_sources)
        # that needs to create directories and files for storing some information
        # e.g., CI_SERVER_TLS_CA_FILE that may be used in next steps.
        # For allowing next steps to access to these files, permissions must be
        # independant from the user that has created them.
        # That's the role of umask 0000.
        run_command = "umask 0000;bash"
    else:
        assert docker is not None  # for mypy
        entrypoint_str = os.environ.get("entrypoint")
        if entrypoint_str is None:
            entrypoint = []
        else:
            entrypoint = json.loads(entrypoint_str)
        run_list = ["docker", "run", "-i"]
        run_list.extend(["-v", "/builds:/builds"])
        # mount global git config for using this gitlab-runner option
        # (safe-directory-checkout)
        run_list.extend(["-v", "/builds/.gitconfig:/etc/gitconfig"])
        run_list.extend(["-v", "/tmp:/tmp"])
        run_list.extend(["-v", "/var/run/docker.sock:/var/run/docker.sock"])
        if entrypoint:
            run_list.extend(["--entrypoint", entrypoint[0]])
        run_list.append(docker)
        run_list.extend(entrypoint[1:])
        run_list.extend(
            [
                "sh",
                "-c",
                """\
if [ -x /usr/local/bin/bash ]; then
	exec /usr/local/bin/bash
elif [ -x /usr/bin/bash ]; then
	exec /usr/bin/bash
elif [ -x /bin/bash ]; then
	exec /bin/bash
elif [ -x /usr/local/bin/sh ]; then
	exec /usr/local/bin/sh
elif [ -x /usr/bin/sh ]; then
	exec /usr/bin/sh
elif [ -x /bin/sh ]; then
	exec /bin/sh
elif [ -x /busybox/sh ]; then
	exec /busybox/sh
else
	echo shell not found
	exit 1
fi

""",
            ]
        )
        run_command = shlex.join(run_list)

    if docker:
        if step_name == "prepare_script":
            # Docker login if docker image
            ci_registry = os.environ.get("CUSTOM_ENV_CI_REGISTRY")
            if ci_registry:
                docker_login(connection, ci_registry)

        # If docker is used, change `/builds` folder owner to match the container
        # user id as scripts are executed inside container
        elif step_name in ["build_script", "step_script"]:
            connection.run(f"sudo chown -R `docker run {docker} id -u` /builds")

    base.make_connection_in_stream_fast(connection)
    with open(command_file) as in_stream:
        try:
            connection.run(run_command, in_stream=in_stream)

        except invoke.exceptions.UnexpectedExit as e:
            exit_code = e.result.exited
            logger.exception(f"Exit code {exit_code}")
            sys.exit(exit_code)

    if docker:
        # If docker is used, once job script finishes, change `/builds` folder owner
        # to match VM (host) id
        # gitlab-runner runs on VM host for cache and artifact operations
        if step_name in ["build_script", "step_script"]:
            connection.run(f"sudo chown -R `id -u` /builds")


if __name__ == "__main__":
    base.execute_with_loggers("run", main)
