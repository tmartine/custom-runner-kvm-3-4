set -ev

RUNNER_TOKEN="$1"
ELASTIC_PASSWORD="$2"
KIBANA_PASSWORD="$3"
RUNNER_NAME="$4"
PROJECT_ID="$5"
PROJECT_ACCESS_TOKEN="$6"


CONCURRENT=32

# Wait till Cloud-Init has finished setting up the image on first boot
echo "Waiting for Cloud-Init..."
if ! cloud-init status -wl ; then
	echo "--------- /var/log/cloud-init-output.log ---------"
	sudo tail -n 20 /var/log/cloud-init-output.log
	echo "--------------------------------------------------"
	exit 1
fi

if [ -f /var/lib/cloud/instance/boot-finished ]; then
    echo Boot finished!
fi

# Initialize docker group

if ! groups | grep -q '\<docker\>'; then
  # Should be performed by cloud-init but seems to be ignored
  sudo usermod -a -G docker ci

  # Restart script with new groups
  exec sudo su ci -c "sh ./deploy_gitlab-runner_on_vm.sh \"$RUNNER_TOKEN\" \"$ELASTIC_PASSWORD\" \"$KIBANA_PASSWORD\" \"$RUNNER_NAME\" \"$PROJECT_ID\" \"$PROJECT_ACCESS_TOKEN\""
fi

# Setup python venv

python3 -m venv orchestrator
orchestrator/bin/pip install --upgrade pip
orchestrator/bin/pip install -r requirements.txt

# Configure docker proxy for local use and btrfs storage driver

# The docker proxy is initialized afterwards, but we configure its use
# now because we restart the docker daemon, and it would stop the
# proxy.

sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<EOF
{
  "registry-mirrors": ["http://localhost:5000"],
  "storage-driver": "btrfs"
}
EOF

RETRY_COUNT=0
while ! sudo systemctl restart docker.service ; do
  sleep 10
  if $((++RETRY_COUNT >= 10)); then
    >&2 echo Too many failure while restarting docker.service.
    exit 1
  fi
done

# Configure swap

if ! sudo [ -f /root/swapfile ] ; then
  sudo dd if=/dev/zero of=/root/swapfile bs=1M count=8192
  sudo chmod 600 /root/swapfile
  sudo mkswap /root/swapfile
  sudo swapon /root/swapfile
fi

# Install docker-registry-proxy service

# https://stackoverflow.com/questions/30930847/how-to-set-up-a-docker-registry-acting-as-a-proxy

if [ ! -f /etc/systemd/system/docker-registry-proxy.service ]; then
  docker run --rm --entrypoint cat registry \
    /etc/docker/registry/config.yml >docker_registry_config.yml
  cat >>docker_registry_config.yml <<EOF
proxy:
  remoteurl: https://registry-1.docker.io
EOF
  sudo install --owner root docker_registry_config.yml /etc/
  sudo install --owner root docker-registry-proxy.service /etc/systemd/system/
fi

sudo systemctl daemon-reload
sudo systemctl start docker-registry-proxy.service

# Install elasticsearch service

docker network create elastic || true
if ! grep --quiet vm.max_map_count /etc/sysctl.conf; then
    sudo tee --append /etc/sysctl.conf >/dev/null <<EOF
vm.max_map_count=262144
EOF
fi
sudo sysctl -w vm.max_map_count=262144

sed -i -e "/^ELASTIC_PASSWORD=/s/=.*\$/=$ELASTIC_PASSWORD/;/^KIBANA_PASSWORD=/s/=.*\$/=$KIBANA_PASSWORD/;" /builds/elasticsearch/.env

mkdir -p /builds/elasticsearch/logs

sudo install --owner root elasticsearch.service /etc/systemd/system/

sudo systemctl daemon-reload
sudo systemctl start elasticsearch.service

# Install log_server service

sudo install --owner root log_server.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl restart log_server

# Install destroy_vms_after_jobs service

sudo install --owner root destroy_vms_after_jobs.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl restart destroy_vms_after_jobs

# Install sanity checker service

sudo install --owner root sanity_checker.service /etc/systemd/system/
sudo install --owner root sanity_checker.timer /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable sanity_checker.timer
sudo systemctl start sanity_checker.timer

# Setup runner

RUNNER_COUNT=$(sudo gitlab-runner list 2>&1 | tail -n+3 | wc -l)

# If there is no runner
if [ "$RUNNER_COUNT" -eq 0 ]; then
  # if there is no authentication token set
  if [ ! -n "$RUNNER_TOKEN" ]; then
    # create runner configuration
    # response is under form {"id":6888,"token":"glrt-zqHKpvzwCirw66URnjsy","token_expires_at":null}%
    RUNNER_TOKEN=$(curl --silent --request POST --url "https://gitlab.inria.fr/api/v4/user/runners" \
      --data "runner_type=project_type" \
      --data "project_id=$PROJECT_ID" \
      --data "description=custom runner orchestrator" \
      --data "tag_list=$RUNNER_NAME,linux,macos,windows,docker,small,medium,large" \
      --header "PRIVATE-TOKEN: $PROJECT_ACCESS_TOKEN" | jq --raw-output '.token' )
  fi

  # connect runner to gitlab instance
  sudo gitlab-runner register --non-interactive \
    --executor "custom" \
    --builds-dir "/builds" \
    --cache-dir "/cache" \
    --safe-directory-checkout true \
    --custom-config-exec "/builds/config.py" \
    --custom-prepare-exec "/builds/prepare.py" \
    --custom-run-exec "/builds/run.py" \
    --custom-cleanup-exec "/builds/cleanup.py" \
    --url "https://gitlab.inria.fr" \
    --token "${RUNNER_TOKEN}"

  sudo sed -i "s/^concurrent = 1\$/concurrent = $CONCURRENT/" \
    /etc/gitlab-runner/config.toml

  sudo systemctl restart gitlab-runner

  # register RUNNER_TOKEN as a variable in the gitlab project
  # thus, if the custom runner orchestrator is re-deployed in new VM,
  # it can authenticate to the already created runner.
  curl --silent --request POST --url "https://gitlab.inria.fr/api/v4/projects/$PROJECT_ID/variables" \
     --form "key=RUNNER_TOKEN" --form "value=$RUNNER_TOKEN" \
     --form "masked=true" --form "protected=true" --form "raw=true" \
     --header "PRIVATE-TOKEN: $PROJECT_ACCESS_TOKEN" | jq --raw-output '.token'
fi
