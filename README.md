# custom-runner

This repository houses a GitLab runner tagged as
`inria-ci/custom-runner`.  This runner is currently in *beta* and is not
declared as a proper shared runner yet, but it is not locked to this
project: all members of this project can use this runner in all their
other projects (similarly to the "qualif" phase we had for shared
runners).

## About the custom runner

This repository offers a GitLab runner that initiates job on virtual
machines created dynamically in the CloudStack instance provided by
ci.inria.fr.

The name *custom* comes from the fact that the implementation relies
on GitLab [Custom executor].

[Custom executor]: https://docs.gitlab.com/runner/executors/custom.html

In comparison with the shared runners previously available on
`gitlab.inria.fr`, the custom runner provides the following
functionalities.

- jobs can run inside a Docker container or directly in a virtual
  machine with Linux, Mac OS X or Windows,

- more computing and storage resources are available, and the size of
  virtual machines hosting jobs can be customized,

- jobs have access to a persistent cache, that is shared with all the
  jobs of the same project.

## How to test the custom runner?

To test the custom runner, feel free to ask ci-staff@inria.fr to add
you to the project. You will then be able to enable the custom runner
on any project you are member of.

To enable the custom runner in a project:

- go to the **Settings** entry on GitLab side pane,

- choose the **CI/CD** sub-item,

- expand the **Runners** section,

- search for `inria-ci-custom-runner-orchestrator` in the list **Other
  available runners**,

- and click on **Enable for this project**.

The custom runner runs all jobs with at least one tag among `linux`,
`macos`, `windows`, `small`, `medium`, `large` or
`inria-ci/custom-runner`.

If you already used shared runners, the experience with the custom
runner should be similar.  Jobs tagged with `small`, `medium` or
`large` will be executed by the custom runner, in the same manner they
were executed by the shared runners.  Jobs with the `linux` tag will
only take into account the value of the `image` field if they are also
carry the `docker` tag.  Refer to the [OS specification with
tags](#os-specification-with-tags) section for more details.
Jobs explicitly tagged with `ci.inria.fr` will
only be executed by the former shared runners: you will need to rename
the tag to `inria-ci/custom-runner` to use the custom runner.

Please report any issue on the
[tracker](https://gitlab.inria.fr/inria-ci/custom-runner/-/issues).

## Image syntax

The runner can run jobs with the following conventions for their
`image` key:

- `featured:<template-name>` for using featured templates on
  CloudStack, such as `featured:ubuntu-20.04-lts` or
  `featured:CI-MacOSX-Catalina-10.15.4-xcode`,

- `community:<template-name>` for using community templates on
  CloudStack, such as `community:ubuntu-2204-64-server`
  or `community:windows-10-wsl`,

- `<docker-image-name>` for using docker images to be run in a container (see
  below).

## Image semantics

A template is instantiated in a fresh virtual machine for each job.
By default, templates are instantiated in the CloudStack project,
`custom-runner`, hosted on ci.inria.fr. It currently offers quotas of
64 cores, 256 GB of RAM, 1 TB of disk storage, and 32 jobs running
concurrently. Note that these quotas may expand in the future.

[custom]: https://gitlab.inria.fr/gitlabci_gallery/orchestration/custom
[gitlabci_gallery]: https://gitlab.inria.fr/gitlabci_gallery/

If an NFS client is available on the VM (see template requirements),
the directory `/cache` is mounted by NFS, is persistent, and is shared
by all the VMs of the same GitLab project.  This directory is intended
to be used via [GitLab CI/CD cache facilities].

[GitLab CI/CD cache facilities]: https://docs.gitlab.com/ee/ci/caching/

## Virtual machine specification

VMs can be customized by adding the following options to the template name,
separated by commas (`,`):
- `cpu:<n>` to set the number of cores
  (e.g., `featured:ubuntu-20.04-lts,cpu:16`),
  the number of cores should be between 1 and 16,
- `ram:<n>` to set the amount of memory in GB
  (e.g., `featured:ubuntu-20.04-lts,cpu:8,ram:16`),
  the amount of memory should be between 1 GB and 24 GB,
- `disk:<n>` to set the root disk size in GB
  (e.g., `featured:ubuntu-20.04-lts,disk:90`),
  the root disk size should be between 1 GB and 100 GB.

If some or all of these parameters are left unspecified, the default
specifications are 2 cores, 8 GB of RAM, and 16 GB for the root disk
unless the job carries one of the tags `small`, `medium` or `large`
(these tags are mutually exclusive: it is an error if more than one of
these tags appear for the same job).

If the job carries one of the tags `small`, `medium` or `large`, then
the default values are the following ones.

- if the job is tagged `small`, the default values are 1 core, 2 GB of
RAM and 16 GB for root disk,
- if the job is tagged `medium`, the default values are 2 cores, 4 GB of
RAM and 16 GB for root disk,
- if the job is tagged `large`, the default values are 4 cores, 8 GB of
RAM and 16 GB for root disk.

They can still be overridden by adding the above options to the
template name.

If the template requires a bigger root disk, the root disk size will
be automatically augmented to fit the template.

## OS specification with tags

Jobs tagged with `linux`, `macos`, or `windows` will be executed with
the following images, ignoring the value of the `image` field
(unless the `docker` tag is used):

- `linux`: `community:ci.inria.fr/ubuntu-23.04/amd64`,
- `macos`: `featured:CI-MacOSX-Catalina-10.15.4-xcode`,
- `windows`: `community:windows-10-wsl`.

The value of the image field is disregarded when tags such as linux,
macos, or windows are present (unless paired with a `docker` tag). This
is because it is a common practice to define an `image` field globally
in the specification file `.gitlab-ci.yml`, and we usually expect the
key `image` to be ignored with jobs tagged with `windows` or `macos`.

The `docker` tag is not supported with the tags `macos` or `windows`
(a future extension could add support for Docker on Windows).  If the
`docker` tag is used in conjunction with the `linux` tag, then the
`linux` tag is just ignored and the behavior is the same as with the
`docker` tag alone.

## Template requirements

Templates should allow SSH connections using the username `ci` and
password `ci`. Additionally, they should offer `bash` support,
implying that Windows subsystem for Linux is mandatory for Windows
templates.

If artifacts are used, the `gitlab-runner` command should be available
in the virtual machine (see the documentation
[Install GitLab Runner](https://docs.gitlab.com/runner/install)).

Mounting `/cache` volume needs NFS client to be available on the VM
and the user `ci` should be *sudoer* and UID 1000.

## Docker

The job can be executed in a Docker container:

- implicitly, when the `image` key is a valid docker image (for instance:
  `ubuntu` or `ubuntu:stable`) and it is not starting with `featured:`,
  `community:`. The virtual machine will by default be based on the
  template `community:ci.inria.fr/alpine-3.18.2-runner/amd64`.

- explicitly, by using a specific cloudstack image and adding the option
  `docker:<image>`, for instance: `community:ci.inria.fr/alpine-3.18.2-runner/amd64,docker:ubuntu`

In both cases, `cpu`, `ram` and `disk` options can be added to customise de VM.
For example, a job using the docker image `ubuntu` in a lightweight `Alpine
Linux` VM with 8 CPUs and the default RAM and disk settings, may be run using:
```
ubuntu,cpu:8
community:ci.inria.fr/alpine-3.18.2-runner/amd64,cpu:8,docker:ubuntu
```

Each docker job is run in a dedicated VM exclusively allocated for the job.

A Docker container can only be used with virtual machine templates
where `docker` is installed.

If the job carries the `docker` tag, then the template specification
is required to specify a Docker image (either with the `docker:`
option or directly).

## Warning

The custom-runner is still a work in progress, and the infrastructure upon which it currently runs is limited.

Sometimes it won't work and will display a message:

````
CloudStack deployment is disabled. Please contact a CI administrator: pfo-staff@inria.fr. Sorry for the inconvenience.
WARNING: Cleanup script failed: exit status 1
ERROR: Job failed: exit status 1
````

This is the implementation of an imperfect solution to the fact resource are limited: we made the choice to forbid deployment when the limit has been reached, as keeping it would further clog the system without providing advantage for the users (as the job would timeout anyway after an hour in the queue) and would spam the infrastructure team.

There are plans to improve the infrastructure behind the custom runner, but they are not validated yet and won't be active before months.

Infrastructure team is working hard to limit as much as possible the inconvenience, but please be patient whenever such problems arise.

## Acknowledgments

The development of this custom runner began during Ihsane Merrir's
internship, supervised by Thierry Martinez and the CI team.
